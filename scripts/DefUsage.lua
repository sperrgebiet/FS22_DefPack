-- sperrgebiet 2022
-- Unlike others I see the meaning in mods that others can learn, expand and improve them. So feel free to use this in your own mods, 
-- add stuff to it, improve it. Your own creativity is the limit ;) If you want to mention me in the credits fine. If not, I'll live happily anyway :P
-- Yeah, I know. I should do a better job in document my code... Next time, I promise... 

DEFUsage = {}

-- It's great that Giants gets rid of functions as part of an update. Now we can do things more complicated than before
--DEFUsage.ModName = g_currentModName
--DEFUsage.ModDirectory = g_currentModDirectory
DEFUsage.ModName = "FS22_DefPack"
DEFUsage.ModDirectory = g_modManager.nameToMod.FS22_DefPack.modDir
DEFUsage.Version = "1.0.0.2"

DEFUsage.Multiplier = 1.5

DEFUsage.defBarColor = {0.270, 0.550, 0.880, 1}
DEFUsage.hudAtlasPath = "dataS/menu/hud/hud_elements.png"

function DEFUsage.prerequisitesPresent(specializations)
    return SpecializationUtil.hasSpecialization(Motorized, specializations)
end

function DEFUsage.registerFunctions(vehicleType)
	SpecializationUtil.registerFunction(vehicleType, "updateDefHud", DEFUsage.updateDefHud)
	SpecializationUtil.registerFunction(vehicleType, "getVehicleDefLevelAndCapacity", DEFUsage.getVehicleDefLevelAndCapacity)
end

function DEFUsage.registerEventListeners(vehicleType)
	SpecializationUtil.registerEventListener(vehicleType, "onPostLoad", 	DEFUsage)
	SpecializationUtil.registerEventListener(vehicleType, "onDraw", 		DEFUsage)
	SpecializationUtil.registerEventListener(vehicleType, "onDelete", 		DEFUsage)
end

function DEFUsage:onPostLoad(savegame)
	DEFUsage.initDefUsage(DEFUsage)
	
	local spec = self.spec_defUsage
	local consumer = self.spec_motorized.consumersByFillType[FillType.DEF]
	if consumer ~= nil then
		spec.isDefActive = true
	end
	
end

function DEFUsage:onDraw(isActiveForInput, isActiveForInputIgnoreSelection)
	if self.spec_enterable.isEntered and self.spec_defUsage.isDefActive then
		self:updateDefHud()
		DEFUsage.defBarElement:draw()
		DEFUsage.defIconElement:draw()
	end
end

function DEFUsage:onDelete()
	local spec = self.spec_defUsage
	if spec.defBarElement ~= nil then
		spec.defBarElement:delete()
		self.defIconElement:delete()
	end
end

function DEFUsage.initDefUsage(self)
	if self.isInitialized ~= true then
		self.defBarElement = DEFUsage.createDefBar(DEFUsage, -1, -1)
		self.defIconElement = DEFUsage.createDefIcon(DEFUsage, -1, -1)

		self.defIconX = x
		self.defIconY = y

		self.isInitialized = true
	end
end

function DEFUsage.updateDefUsageFactor(self)
    local spec = self.spec_motorized
    local consumer = spec.consumersByFillType[FillType.DEF]
	if consumer ~= nil then
		consumer.usage = consumer.usage * DEFUsage.Multiplier
	end
end

function DEFUsage.createDefBar(self, posX, posY)
	local width, height = getNormalizedScreenValues(unpack(SpeedMeterDisplay.SIZE.FUEL_LEVEL_BAR))
	local element = HUDRoundedBarElement.new(self.hudAtlasPath, posX, posY, width, height, false)

	element:setBarColor(unpack(self.defBarColor))
	return element
end

function DEFUsage.createDefIcon(self, posX, posY)
	local width, height = getNormalizedScreenValues(unpack(SpeedMeterDisplay.SIZE.FUEL_LEVEL_ICON))
	--local defIcon = g_fillTypeManager.nameToFillType['DEF'].hudOverlayFilename
	local defIcon = Utils.getFilename("textures/hud_fill_def.dds", self.ModDirectory)
	--local iconOverlay = Overlay.new(defIcon, posX, posY, width * 0.75, height * 0.75)
	local iconOverlay = Overlay.new(tostring(defIcon), posX, posY, width, height)
	--iconOverlay:setColor(unpack(self.iconFillColor))

	local defIconElement = HUDElement.new(iconOverlay)		
	return defIconElement
end

function DEFUsage.getVehicleDefLevelAndCapacity(vehicle)
    local defFillType = vehicle:getConsumerFillUnitIndex(FillType.DEF)
    local level = vehicle:getFillUnitFillLevel(defFillType)
    local capacity = vehicle:getFillUnitCapacity(defFillType)
    return level, capacity
end

function DEFUsage:updateDefHud()
	local x = g_currentMission.inGameMenu.hud.speedMeter.fuelBarElement.barOverlayMiddle.x + g_currentMission.inGameMenu.hud.speedMeter.fuelBarElement.barOverlayMiddle.width
	local y = g_currentMission.inGameMenu.hud.speedMeter.fuelBarElement.barOverlayMiddle.y	
	DEFUsage.defBarElement:setPosition(x, y)
		
	x = g_currentMission.inGameMenu.hud.speedMeter.fuelGaugeIconElement.overlay.x + ( g_currentMission.inGameMenu.hud.speedMeter.fuelGaugeIconElement.overlay.width * 1.5 )
	y = g_currentMission.inGameMenu.hud.speedMeter.fuelGaugeIconElement.overlay.y - ( g_currentMission.inGameMenu.hud.speedMeter.fuelGaugeIconElement.overlay.height / 1.5 )
	DEFUsage.defIconElement:setPosition(x, y)

	local level, capacity = self.getVehicleDefLevelAndCapacity(self)
	if capacity > 0 then
		DEFUsage.defBarElement:setValue(level / capacity, "FUEL")
	else
		DEFUsage.defBarElement:setValue(1)
	end
end

function DEFUsage:onFillUnitFillLevelChanged(self, superFunc, fillUnitIndex, fillLevelDelta, fillType, toolType, fillPositionData, appliedDelta)
    -- We don't want to refill def if diesel is refilled
end

Motorized.onFillUnitFillLevelChanged = Utils.overwrittenFunction(Motorized.onFillUnitFillLevelChanged, DEFUsage.onFillUnitFillLevelChanged)